%global use_sqlcipher 1

%if 0%{?suse_version}
%define __builder ninja
%endif

Name:             sqlitebrowser
Summary:          Create, design, and edit SQLite database files
Version:          3.12.2
Release:          1%{?dist}
License:          GPLv3+ or MPLv2.0
URL:              https://sqlitebrowser.org/
Source0:          https://github.com/%{name}/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz

# Patch0 written by Sandro Mani (https://src.fedoraproject.org/user/smani).
# Unbundle bundled libraries.
Patch0:           sqlitebrowser-unbundle-fedora.patch
Patch1:           sqlitebrowser-unbundle-opensuse.patch

BuildRequires:    cmake
BuildRequires:    gcc-c++

%if 0%{?fedora}
BuildRequires:    ninja-build
BuildRequires:    pkgconfig(qcustomplot-qt5)
BuildRequires:    pkgconfig(qhexedit2-qt5)
BuildRequires:    qscintilla-qt5-devel
BuildRequires:    qt5-qtbase-devel
BuildRequires:    qt5-qttools-devel
%endif
%if 0%{?suse_version}
BuildRequires:    ninja
BuildRequires:    qcustomplot-devel
BuildRequires:    qhexedit2-devel
BuildRequires:    libqscintilla_qt5-devel
BuildRequires:    libqt5-qtbase-devel
BuildRequires:    libqt5-qttools-devel
%endif

BuildRequires:    pkgconfig(sqlite3)

%if 0%{?use_sqlcipher}
BuildRequires:    pkgconfig(sqlcipher)
%endif

%description
SQLite Database Browser is a high quality, visual, open source tool to create,
design, and edit database files compatible with SQLite.

%if 0%{?suse_version}
%debug_package
%endif

%prep
%setup -q
%if 0%{?fedora}
%patch0 -p1
%endif
%if 0%{?suse_version}
%patch1 -p1
%endif

# Unbundle bundled libraries, but leave QScintilla for openSUSE Leap.
%if 0%{?fedora} || 0%{?suse_version} >= 1550
rm -rf libs/{qcustomplot-source,qhexedit,qscintilla}
%endif
%if 0%{?is_opensuse} && 0%{?suse_version} < 1550
rm -rf libs/{qcustomplot-source,qhexedit}
%endif

%build
%cmake \
%if 0%{?use_sqlcipher}
    -Dsqlcipher=1 \
%endif
%if 0%{?is_opensuse} && 0%{?suse_version} < 1550
    -DFORCE_INTERNAL_QSCINTILLA=ON \
%endif
    -DFORCE_INTERNAL_QCUSTOMPLOT=OFF \
    -DFORCE_INTERNAL_QHEXEDIT=OFF \
    -DQT_INCLUDE_DIR=%{_includedir}/qt5 \
    -GNinja \
    .
%cmake_build

%install
%cmake_install

%files
%doc README.md
%license LICENSE
%{_bindir}/%{name}
%{_datadir}/metainfo/%{name}.desktop.appdata.xml
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/256x256/apps/%{name}.png
